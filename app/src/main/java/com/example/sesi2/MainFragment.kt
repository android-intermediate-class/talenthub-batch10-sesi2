package com.example.sesi2

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.sesi2.databinding.FragmentMainBinding
import com.example.sesi2.extension.gone
import com.example.sesi2.extension.showToast
import com.example.sesi2.extension.visible
import java.util.regex.Pattern

class MainFragment : Fragment() {

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!
    private val tag = "check lifecycle"
    private val tagListener = "check listener"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i(tag, "onCreate main fragment executed")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        Log.i(tag, "onCreateView main fragment executed")
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.i(tag, "onViewCreated main fragment executed")


        binding.ivLogo.setOnTouchListener { _, event ->
                when (event.action) {
                    MotionEvent.ACTION_DOWN -> {
                        Log.i(tagListener, "ACTION_DOWN main fragment executed")
                        true
                    }
                    MotionEvent.ACTION_MOVE -> {
                        Log.i(tagListener, "ACTION_MOVE main fragment executed")
                        true
                    }
                    MotionEvent.ACTION_UP -> {
                        Log.i(tagListener, "ACTION_UP main fragment executed")
                        true
                    }
                    else -> false
                }
            }

        binding.tieEmail.setOnFocusChangeListener { view, hasFocus ->
            if (!hasFocus) {
                // Validasi alamat email saat kehilangan fokus
                validateEmail(binding.tieEmail.text.toString())
            }
        }


        binding.tieNoHp.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                // Tidak ada tindakan sebelum perubahan teks
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                // Tidak ada tindakan saat teks berubah
            }

            override fun afterTextChanged(s: Editable?) {
                s?.let {
                    // Hapus karakter selain angka
                    val digits = it.toString().replace("[^\\d]".toRegex(), "")

                    if (digits.length >= 4) {
                        val formattedNumber = StringBuilder()
                        formattedNumber.append("(").append(digits.substring(0, 3)).append(") ")
                        if (digits.length >= 7) {
                            formattedNumber.append(digits.substring(3, 6)).append("-")
                            formattedNumber.append(digits.substring(6))
                        } else {
                            formattedNumber.append(digits.substring(3))
                        }
                        binding.tieNoHp.removeTextChangedListener(this)
                        binding.tieNoHp.setText(formattedNumber.toString())
                        binding.tieNoHp.setSelection(formattedNumber.length)
                        binding.tieNoHp.addTextChangedListener(this)
                    }
                }
            }
        })

        binding.button1.setOnLongClickListener {
            requireContext().showToast("setOnLongClickListener main fragment executed")
            true
        }

        binding.buttonSubmit.setOnClickListener {
            requireContext().startActivity(Intent(requireContext(), SecondActivity::class.java))
        }
    }

    override fun onStart() {
        super.onStart()
        Log.i(tag, "onStart main fragment executed")
    }

    override fun onResume() {
        super.onResume()
        Log.i(tag, "onResume main fragment executed")
    }

    override fun onPause() {
        super.onPause()
        Log.i(tag, "onPause main fragment executed")
    }

    override fun onStop() {
        super.onStop()
        Log.i(tag, "onStop main fragment executed")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Log.i(tag, "onSaveInstanceState main fragment executed")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.i(tag, "onDestroyView main fragment executed")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i(tag, "onDestroy main fragment executed")
        _binding = null
    }

    private fun validateEmail(email: String, ) {
        val email = email.trim()

        if (isValidEmail(email)) {
            binding.tilEmail.error = ""
            binding.buttonSubmit.visible()
        } else {
            binding.tilEmail.error = "Invalid Email"
            binding.buttonSubmit.gone()
        }
    }

    private fun isValidEmail(email: String): Boolean {
        val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
        val pattern = Pattern.compile(emailPattern)
        val matcher = pattern.matcher(email)
        return matcher.matches()
    }
}